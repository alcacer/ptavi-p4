#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import sys
import json
from time import time, strftime, gmtime


class SIPRegisterHandler(socketserver.DatagramRequestHandler):

    dicc = {}

    def json2register(self):

        try:
            with open("registered.json", "r") as jsonfile:
                self.dicc = json.load(jsonfile)
                print("Actualizamos página")

        except FileNotFoundError:
            pass

    def register2json(self):
        fich = "registered.json"
        with open(fich, "w") as fiche:
            json.dump(self.dicc, fiche, indent=3)

    def handle(self):
        self.json2register()
        text = self.rfile.read()
        SERVER = self.client_address[0]
        PORT = self.client_address[1]
        INFO = ' SERVER: ' + str(SERVER) + ' ' + ' PORT: ' + str(PORT)
        REGIST = text.decode('utf-8').split(" ")[0]
        MAIL = text.decode('utf-8').split(" ")[2]
        EXP = text.decode('utf-8').split(" ")[3].split("\r\n")[1].split(":")[1]
        ZONE = "%Y-%m-%d %H:%M:%S"
        SUMA = int(EXP) + int(time())
        HORA = strftime(ZONE, gmtime(SUMA))
        ACTUAL = strftime(ZONE, gmtime(time()))
        self.wfile.write(b"Hemos recibido tu peticion")

        if REGIST == 'register':
            self.dicc[MAIL] = INFO, HORA

        if int(EXP) == 0:
            print("se eliminara " + MAIL)
            del self.dicc[MAIL]

        for MAIL in self.dicc.copy():
            if self.dicc[MAIL][1] <= ACTUAL:
                del self.dicc[MAIL]

        for text in self.rfile:
            print("El cliente nos manda ", text.decode('utf-8'))
        print(INFO)
        print(self.dicc)
        self.register2json()


if __name__ == "__main__":

    PORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
