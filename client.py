#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

try:

    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    REGIS = str(sys.argv[3])
    MAIL = str(sys.argv[4])
    EXP = int(sys.argv[5])
    INFO = REGIS + " SIP " + MAIL + " SIP/2.0""\r\n" + "Expires:" + str(EXP)+"\r\n"


except IndexError:
    sys.exit("Escribe : python3 client.py IP puerto 'register' email y tiempo")


with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", INFO)
    my_socket.send(bytes(INFO, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
